import React, { useReducer } from 'react';
import DataContext from './dataContext';
import { dataReducer } from './dataReducer';
import { GET_LANG, HANDLE_FORM } from './types';

const DataState = (props) => {
  const initialState = {
    lang: null,
    formState: 'none',
  };
  const [state, dispatch] = useReducer(dataReducer, initialState);
  // Global Axios Header

  const getLang = (lang) => {
    try {
      dispatch({
        type: GET_LANG,
        payload: lang,
      });
    } catch (err) {
      console.log(err);
    }
  };

  const openHandleState = (open) => {
    dispatch({
      type: HANDLE_FORM,
      payload: open,
    });
  };

  return (
    <DataContext.Provider
      value={{
        getLang,
        lang: state.lang,
        formState: state.formState,
        openHandleState,
      }}
    >
      {props.children}
    </DataContext.Provider>
  );
};
export default DataState;
