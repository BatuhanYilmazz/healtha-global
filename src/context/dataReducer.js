import { GET_LANG, HANDLE_FORM } from './types';

export const dataReducer = (state, action) => {
  switch (action.type) {
    case GET_LANG:
      return {
        ...state,
        lang: action.payload,
      };
    case HANDLE_FORM:
      return {
        ...state,
        formState: action.payload,
      };

    default:
      return state;
  }
};
