import React, { useContext, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { NavbarStyled } from "../styles/components";
import { handleScroll } from "../animations/animation";
import { useTranslation } from "react-i18next";
import DataContext from "../context/dataContext";
import EventListener, { withOptions } from "react-event-listener";
import NavbarSocial from "./NavbarSocial";
import Brand from "./Brand";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTwitter,
  faFacebook,
  faInstagram,
  faLinkedinIn,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import $ from "jquery";
import { faBars } from "@fortawesome/free-solid-svg-icons";
function Navbar() {
  const { t, i18n } = useTranslation();
  let lang = localStorage.getItem("i18nextLng");
  const dataContext = useContext(DataContext);
  const { getLang } = dataContext;

  const handleClick = (language) => {
    i18n.changeLanguage(language);
  };

  useEffect(() => {
    getLang(lang);
    // eslint-disable-next-line
  }, [lang]);

  $(".navbar-collapse .nav-drop").click(function () {
    $(".navbar-collapse").collapse("hide");
  });

  return (
    <NavbarStyled>
      <EventListener
        target='window'
        onScroll={withOptions(handleScroll, {
          passive: true,
          capture: false,
        })}
      />
      <div className='navbar-animate'>
        <NavbarSocial />
        <div className='container'>
          <nav className='navbar navbar-expand-lg'>
            <NavLink className='navbar-brand' to='/'>
              <Brand />
            </NavLink>
            <button
              className='navbar-toggler'
              type='button'
              data-toggle='collapse'
              data-target='#navbarSupportedContent'
              aria-controls='navbarSupportedContent'
              aria-expanded='false'
              aria-label='Toggle navigation'
            >
              <FontAwesomeIcon icon={faBars} color='#47b9bb' />
            </button>

            <div
              className='collapse navbar-collapse'
              id='navbarSupportedContent'
            >
              <ul className='navbar-nav ml-auto'>
                <li className='nav-item dropdown'>
                  <NavLink
                    className='nav-link dropdown-toggle'
                    to='#'
                    id='navbarDropdown'
                    role='button'
                    data-toggle='dropdown'
                    aria-haspopup='true'
                    aria-expanded='false'
                  >
                    {t("SAC_EKIMI")}
                  </NavLink>
                  <div
                    className='dropdown-menu'
                    aria-labelledby='navbarDropdown'
                  >
                    <NavLink
                      className='dropdown-item nav-drop'
                      to={{ pathname: "/sac-ekimi", state: "dhi" }}
                    >
                      {t("DHI")}
                    </NavLink>
                    <NavLink
                      className='dropdown-item nav-drop'
                      to={{ pathname: "/sac-ekimi", state: "fue" }}
                    >
                      {t("FUE")}
                    </NavLink>
                    <NavLink
                      className='dropdown-item nav-drop'
                      to={{ pathname: "/sac-ekimi", state: "organicsacekimi" }}
                    >
                      {t("ORGANIC_SAC_EKIMI")}
                    </NavLink>
                  </div>
                </li>
                <li className='nav-item dropdown'>
                  <NavLink
                    className='nav-link dropdown-toggle'
                    to='#!'
                    id='navbarDropdown'
                    role='button'
                    data-toggle='dropdown'
                    aria-haspopup='true'
                    aria-expanded='false'
                  >
                    {t("ESTETIK_PLASTIK_CERRAHI")}
                  </NavLink>
                  <div
                    className='dropdown-menu'
                    aria-labelledby='navbarDropdown'
                  >
                    <div className='d-md-flex align-content-center justify-content-lg-center'>
                      <div>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "burunestetiği",
                          }}
                        >
                          {t("BURUN_ESTETİĞİ")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "ikincilburunestetiği",
                          }}
                        >
                          {t("İKİNCİL_BURUN_ESTETİĞİ")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "tvş",
                          }}
                        >
                          {t("TÜM_VUCUT_SEKİLLENDİRME")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "bbl",
                          }}
                        >
                          {t("BBL")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "spider",
                          }}
                        >
                          {t("ÖRÜMCEK_AĞI_TEKNİĞİ")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "facelift",
                          }}
                        >
                          {t("YÜZ_KALDIRMA")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "gözlift",
                          }}
                        >
                          {t("GÖZ_KAPAĞI_AMELİYATI")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "armlift",
                          }}
                        >
                          {t("KOL_KALDIRMA")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "bacakgerme",
                          }}
                        >
                          {t("BACAK_GERME")}
                        </NavLink>
                      </div>
                      <div>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "karıngerme",
                          }}
                        >
                          {t("KARIN_GERME")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "lipo",
                          }}
                        >
                          {t("LIPOSUCTION")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "bodylifting",
                          }}
                        >
                          {t("BODY_LIFTING")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "dudakkaldırma",
                          }}
                        >
                          {t("DUDAK_KALDIRMA")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "memebüyütme",
                          }}
                        >
                          {t("MEME_BUYUTME")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "memekucultme",
                          }}
                        >
                          {t("MEME_KUCULTME")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "memekaldırma",
                          }}
                        >
                          {t("MEME_KALDIRMA")}
                        </NavLink>
                        <NavLink
                          className='dropdown-item nav-drop'
                          to={{
                            pathname: "/estetik-plastik-cerrahi",
                            state: "erkekmemekucultme",
                          }}
                        >
                          {t("ERKEK_MEME_KUCULTME")}
                        </NavLink>
                      </div>
                    </div>
                  </div>
                </li>

                <li className='nav-item'>
                  <NavLink
                    className='nav-link nav-drop'
                    to={{
                      pathname: "/obezite-metabolik-cerrahi",
                      state: "organicsacekimi",
                    }}
                  >
                    {t("OBEZITE_METABOLİK_CERRAHI")}
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    className='nav-link nav-drop'
                    to={{
                      pathname: "/agız-dis-saglıgı",
                    }}
                  >
                    {t("AĞIZ_VE_DİŞ_SAGLIGI")}
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    className='nav-link'
                    to={{
                      pathname: "/kulak-burun-bogaz",
                    }}
                  >
                    {t("KBB")}
                  </NavLink>
                </li>
                <li className='nav-item dropdown'>
                  <NavLink
                    className='nav-link dropdown-toggle'
                    to='#!'
                    id='navbarDropdown'
                    role='button'
                    data-toggle='dropdown'
                    aria-haspopup='true'
                    aria-expanded='false'
                  >
                    {t("GÖZ_SAGLIGI")}
                  </NavLink>
                  <div
                    className='dropdown-menu'
                    aria-labelledby='navbarDropdown'
                  >
                    <NavLink
                      className='dropdown-item nav-drop'
                      to={{
                        pathname: "/goz-saglıgı",
                        state: "intralaselasik",
                      }}
                    >
                      {t("INTRALASE_LASIK")}
                    </NavLink>
                    <NavLink
                      className='dropdown-item nav-drop'
                      to={{
                        pathname: "/goz-saglıgı",
                        state: "wavefrontlasik",
                      }}
                    >
                      {t("WAVEFRONT_LASIK")}
                    </NavLink>
                    <NavLink
                      className='dropdown-item nav-drop'
                      to={{
                        pathname: "/goz-saglıgı",
                        state: "katarakmonofokal",
                      }}
                    >
                      {t("KATARAKT_MONOFOKAL")}
                    </NavLink>
                    <NavLink
                      className='dropdown-item nav-drop'
                      to={{
                        pathname: "/goz-saglıgı",
                        state: "kataraktrikofal",
                      }}
                    >
                      {t("KATARAKT_TRIFOKAL")}
                    </NavLink>
                  </div>
                </li>
                <li className='nav-item'>
                  <NavLink
                    className='nav-link nav-drop'
                    to={{
                      pathname: "/guzellik-merkezi",
                      state: "organicsacekimi",
                    }}
                  >
                    {t("GUZELLİK_MERKEZİ")}
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    className='nav-link border-0 nav-drop'
                    to={{
                      pathname: "/organizasyon-hizmetleri",
                    }}
                  >
                    {t("ORGANIZASYON_HIZMETLERI")}
                  </NavLink>
                </li>
                <li className='nav-item d-lg-none'>
                  <NavLink
                    to={{ pathname: "/about-us", state: "organicsacekimi" }}
                    className='nav-link border-0 nav-drop'
                  >
                    {t("ABOUT_US")}
                  </NavLink>
                </li>
                <li className='nav-item d-lg-none'>
                  <NavLink
                    to={{ pathname: "/gallery", state: "organicsacekimi" }}
                    className='nav-link border-0 nav-drop'
                  >
                    {t("GALERİ")}
                  </NavLink>
                </li>
                <li className='nav-item d-lg-none'>
                  <NavLink
                    to={{ pathname: "/contact-us", state: "organicsacekimi" }}
                    className='nav-link border-0 nav-drop'
                  >
                    {t("İLETİŞİM")}
                  </NavLink>
                </li>
                <li className='nav-item d-lg-none my-3'>
                  <div className='text-left navbar-mobile-social'>
                    <a
                      href='https://www.linkedin.com/company/71556627'
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      <FontAwesomeIcon icon={faLinkedinIn} color='#47b9bb' />
                    </a>
                    <a
                      href='https://www.facebook.com/healthaglobal'
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      <FontAwesomeIcon icon={faFacebook} color='#47b9bb' />
                    </a>
                    <a
                      href='https://twitter.com/GlobalHealtha'
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      <FontAwesomeIcon icon={faTwitter} color='#47b9bb' />
                    </a>
                    <a
                      href='https://www.instagram.com/healthaglobal/'
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      <FontAwesomeIcon icon={faInstagram} color='#47b9bb' />
                    </a>
                    <a
                      href='https://www.youtube.com/channel/UCIAIYuuD09u0Kb44kUddPGg'
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      <FontAwesomeIcon icon={faYoutube} color='#47b9bb' />
                    </a>
                  </div>
                </li>
                <li className='nav-item d-lg-none d-flex align-items-center justify-content-flex-start mobile-nav-social'>
                  <button
                    className={`${lang === "tr" ? "langactive" : ""} nav-drop`}
                    onClick={() => handleClick("tr")}
                  >
                    TR
                  </button>
                  <button
                    className={`${lang === "en" ? "langactive" : ""} nav-drop`}
                    onClick={() => handleClick("en")}
                  >
                    EN
                  </button>
                  <a
                    href='https://ar.healthaglobal.com'
                    className='animate-link'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    AR
                  </a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </NavbarStyled>
  );
}

export default Navbar;
