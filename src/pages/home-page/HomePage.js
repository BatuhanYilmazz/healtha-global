import React from 'react';
import Header from './Header';
//import Testimonials from './Testimonials';
import OurServices from './OurServices';
import Consultancy from './Consultancy';
import WhyTurkey from './WhyTurkey';

function HomePage() {
  return (
    <>
      <Header />
      <WhyTurkey />
      <OurServices />
      <Consultancy />
      {/** <Testimonials /> */}
    </>
  );
}

export default HomePage;
