import React from 'react';
import { OurServicesStyled } from '../../styles/pages';
import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';
function OurServices() {
  const { t } = useTranslation();

  return (
    <OurServicesStyled>
      <div className='container'>
        <h1>{t('OUR_SERVICES')}</h1>
        <div className='row mb-4'>
          <div className='col-sm-12 col-md-6 col-lg-3 position-relative mb-4'>
            <NavLink to={{ pathname: '/sac-ekimi', state: 'dhi' }}>
              <div className='img-hover-zoom'>
                <img src='/images/homepage/sacekimi.jpg' alt='saçekimi' />
                <h6>{t('SAC_EKİMİ_H1')}</h6>
                <div className='overlay'></div>
              </div>
            </NavLink>
          </div>
          <div className='col-sm-12 col-md-6 col-lg-3 position-relative mb-4'>
            <NavLink
              to={{
                pathname: '/estetik-plastik-cerrahi',
                state: 'burunestetiği',
              }}
            >
              <div className='img-hover-zoom'>
                <img
                  src='/images/homepage/estetikveplastikcerrahi.jpg'
                  alt='estetikveplastikcerrahi'
                />
                <h6>{t('ESTETIK_PLASTIK_CERRAHI_H1')}</h6>
                <div className='overlay'></div>
              </div>
            </NavLink>
          </div>
          <div className='col-sm-12 col-md-6 col-lg-3 position-relative mb-4'>
            <NavLink to='/obezite-metabolik-cerrahi'>
              <div className='img-hover-zoom'>
                <img
                  src='/images/homepage/obezitevemetabolikcerrahi.jpg'
                  alt='obezitevemetabolikcerrahi'
                />
                <h6>{t('OBEZITE_METABOLIK_CERRAHI_H1')}</h6>
                <div className='overlay'></div>
              </div>
            </NavLink>
          </div>
          <div className='col-sm-12 col-md-6 col-lg-3 position-relative mb-4'>
            <NavLink to='/kulak-burun-bogaz'>
              <div className='img-hover-zoom'>
                <img src='/images/homepage/kbb.jpg' alt='kulak burun boğaz' />
                <h6>{t('KBB_H1')}</h6>
                <div className='overlay'></div>
              </div>
            </NavLink>
          </div>
          <div className='col-sm-12 col-md-6 col-lg-3 position-relative mb-4'>
            <NavLink to='/agız-dis-saglıgı'>
              <div className='img-hover-zoom'>
                <img src='/images/homepage/diş.jpg' alt='diş sağlığı' />
                <h6>{t('DİS_SAGLIGI_H1')}</h6>
                <div className='overlay'></div>
              </div>
            </NavLink>
          </div>
          <div className='col-sm-12 col-md-6 col-lg-3 position-relative mb-4'>
            <NavLink
              to={{
                pathname: '/goz-saglıgı',
                state: 'intralaselasik',
              }}
            >
              <div className='img-hover-zoom'>
                <img src='/images/homepage/göz.jpg' alt='göz sağlığı' />
                <h6>{t('GÖZ_SAGLIGI_H1')}</h6> <div className='overlay'></div>
              </div>
            </NavLink>
          </div>
          <div className='col-sm-12 col-md-6 col-lg-3 position-relative mb-4'>
            <NavLink to='/guzellik-merkezi'>
              <div className='img-hover-zoom'>
                <img
                  src='/images/homepage/güzellikmerkezi.jpg'
                  alt='güzellikmerkezi'
                />
                <h6>{t('GÜZELLİK_MERKEZİ_H1')} </h6>
                <div className='overlay'></div>
              </div>
            </NavLink>
          </div>

          <div className='col-sm-12 col-md-6 col-lg-3 position-relative mb-4'>
            <NavLink to='/organizasyon-hizmetleri'>
              <div className='img-hover-zoom'>
                <img
                  src='/images/homepage/organizasyon.jpg'
                  alt='organizasyon'
                />
                <h6>{t('ORGANIZASYON_HIZMETLERI_H1')}</h6>
                <div className='overlay'></div>
              </div>
            </NavLink>
          </div>
        </div>
      </div>
    </OurServicesStyled>
  );
}

export default OurServices;
