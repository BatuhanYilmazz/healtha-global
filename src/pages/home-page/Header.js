import React, { useContext } from 'react';
import { HeaderStyled } from '../../styles/pages';
import { useTranslation } from 'react-i18next';
import DataContext from '../../context/dataContext';

function Header() {
  const { t } = useTranslation();
  const dataContext = useContext(DataContext);
  const { openHandleState } = dataContext;
  return (
    <HeaderStyled>
      <div className='row'>
        <div className='col'>
          <div className='content'>
            <h1 className='title'>{t('HEADER_TITLE')}</h1>
            <p className='description'>{t('HEADER_COMMENT')}</p>
            <button onClick={() => openHandleState('block')}>
              {t('CALL_YOU_BACK')}
            </button>
          </div>
        </div>
      </div>
    </HeaderStyled>
  );
}

export default Header;
