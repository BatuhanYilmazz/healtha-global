import React from 'react';
import { WhyTurkeyStyled } from '../../styles/pages';
import { useTranslation } from 'react-i18next';

function WhyTurkey() {
  const { t } = useTranslation();

  return (
    <WhyTurkeyStyled>
      <div className='row'>
        <div className='col'>
          <div className='container'>
            <h1>{t('WHY_TURKEY')}</h1>
            <br />
            <h3>{t('WHY_TURKEY_2')}</h3>
            <br />
            <br />
            <p>{t('WHY_TURKEY_DESC1')}</p>
            <p>{t('WHY_TURKEY_DESC2')}</p>
            <p>{t('WHY_TURKEY_DESC3')}</p>
            <p>{t('WHY_TURKEY_DESC4')}</p>
            <p>{t('WHY_TURKEY_DESC5')}</p>
            <p>{t('WHY_TURKEY_DESC6')}</p>
          </div>
        </div>
      </div>
    </WhyTurkeyStyled>
  );
}

export default WhyTurkey;
